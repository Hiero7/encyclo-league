package com.encyclo.league.managers;

import com.loopj.android.http.AsyncHttpClient;

public class AsyncHttpClientManager {

	private static AsyncHttpClientManager mInstance = null;
	private static AsyncHttpClient mClientInstance = null;


	private AsyncHttpClientManager(){
		mClientInstance = new AsyncHttpClient();
		mClientInstance.setTimeout(1000000);
	}

	public static AsyncHttpClientManager getInstance(){

		if(mInstance == null){
			mInstance = new AsyncHttpClientManager();
		}
		return mInstance;
	}

	public AsyncHttpClient getAsyncHttpClient(){
		return mClientInstance;
	}

}
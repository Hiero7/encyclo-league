package com.encyclo.league.managers;

import java.security.KeyStore;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import org.apache.http.conn.ssl.SSLSocketFactory;
import org.json.JSONObject;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.RequestParams;

public abstract class NetworkManager {

	private final String TAG = NetworkManager.class.getSimpleName();
	
	public static final String API_KEY = "c6c9f5b8-310b-4609-9338-7c7c212c7df3";
	public static final String API_BASE_URL = "https://na.api.pvp.net/api/lol/";
	public static final String DEFAULT_REGION = "na/";
	public static final String DEFAULT_VERSION = "v1.2/";
	
	public static final String API_GET_CHAMPIONS = "champion";
	
	public static final int ChampionManagerGetChampions = 101;
	
	private ArrayList<NetworkNotificationListener> mListeners = null;

	public interface NetworkNotificationListener {
		public void onSuccess(final int methodId);
		public void onSuccessWithResult(final int methodId, final JSONObject response);
		public void onFailure(final int methodId, final Error error);
		public void onAccessDenied(final int methodId);
		public void onProgress(final int methodId, final int progress);
	}

	public void sendSuccessNotifications(int methodId) {
		for(int i = 0;i<mListeners.size();i++) {
			mListeners.get(i).onSuccess(methodId);
		}
	}

	public void sendSuccessNotificationsWithResult(int methodId, JSONObject response) {
		for(int i = 0;i<mListeners.size();i++) {
			mListeners.get(i).onSuccessWithResult(methodId, response);
		}
	}

	public void sendFailureNotifications(int methodId, Error error) {
		for(int i = 0;i<mListeners.size();i++) {
			mListeners.get(i).onFailure(methodId, error);
		}
	}

	public void sendAccessDeniedNotifications(int methodId) {
		for(int i = 0;i<mListeners.size();i++) {
			mListeners.get(i).onAccessDenied(methodId);
		}
	}

	public void sendProgressNotifications(int methodId, int progress) {
		for(int i = 0;i<mListeners.size();i++) {
			mListeners.get(i).onProgress(methodId, progress);
		}
	}
	//set up AsynHTTPClient
	private AsyncHttpClient mClient;
	
	private String getAbsoluteUrl(String relativeUrl) {
		String url = API_BASE_URL + DEFAULT_REGION + DEFAULT_VERSION + relativeUrl + "?api_key="+API_KEY;
		Log.i(TAG, url);
		return url;
	}
	
	public void asyncGet(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		if(params == null) {
			mClient.get(url, responseHandler);
		} else {
			mClient.get(url, params, responseHandler);
		}

	}
	
	public abstract void networkRequestDidFinishWithResponse(final int methodId, final JSONObject responseJSON);
	public abstract void networkRequestDidFinishWithError(final int methodId, final Error error);
	public abstract void networkRequestDidUpdateProgress(final int methodId, final int bytesWritten, final int totalBytes);
	
	protected NetworkManager() {
		AsyncHttpClientManager clientManager = AsyncHttpClientManager.getInstance();
		mClient = clientManager.getAsyncHttpClient();
		mClient.setMaxRetriesAndTimeout(3, 30000);
		mClient.setTimeout(30000);
		mListeners = new ArrayList<NetworkNotificationListener>();
	}
	
//	private class BlackholeCookieStore implements CookieStore {
//        @Override
//        public void addCookie(Cookie cookie) {
//        }
//
//        @Override
//        public List<Cookie> getCookies() {
//            return new ArrayList<Cookie>();
//        }
//
//        @Override
//        public void clear() {
//        }
//
//		@Override
//		public boolean clearExpired(Date date) {
//			// TODO Auto-generated method stub
//			return true;
//		}
//    };
//	
//	public void setAuth(String auth) {
//		mClient.addHeader("Auth-Type", "Token");
//		mClient.addHeader("Auth-Token", auth);
//	}
//	
//	public void clearAuth() {
//		mClient.removeHeader("Auth-Type");
//		mClient.removeHeader("Auth-Token");
//		CookieStore cookieStore = (CookieStore) mClient.getHttpContext().getAttribute(ClientContext.COOKIE_STORE);
//		if(cookieStore!=null) {
//			cookieStore.clear();
//		}
//		BlackholeCookieStore empty = new BlackholeCookieStore();
//		mClient.setCookieStore(empty);
//	}
	
	public void addListener(NetworkNotificationListener listener) {
		int index = mListeners.indexOf(listener);
		if(index<0) {
			mListeners.add(listener);
		}
	}

	public synchronized void removeListener(NetworkNotificationListener listener) {
		int index = mListeners.indexOf(listener);
		if(index>-1) {
			mListeners.remove(index);
		}
	}

	public synchronized void removeAllListeners() {
		for(int i = 0; i<mListeners.size(); i++) {
			mListeners.remove(i);
		}
	}
	
	// code to accept all certificates

		// always verify the host - don't check for certificate
		public final static HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		/**
		 * Trust every server - dont check for any certificate
		 */
		public  void trustAllHosts() {
			KeyStore trustStore = null;
			try {
				trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
				trustStore.load(null, null);
			} catch (Throwable t) {
				t.printStackTrace();
			}
			SSLSocketFactory socketFactory;
			try {
				socketFactory = new MySSLSocketFactory(trustStore);
				socketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			} catch (Throwable t) {
				t.printStackTrace();
				socketFactory = SSLSocketFactory.getSocketFactory();
			}

			mClient.setSSLSocketFactory(socketFactory);
		}
	
		public void makeGetServiceRequest(String uri, RequestParams params, final int methodId) {
			trustAllHosts();

			asyncGet(uri, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode,  org.apache.http.Header[] headers, byte[] responseBody) {
					Log.v(TAG, "Successfully got");
					try {
						String responseMessage = new String(responseBody, "UTF-8");
						Log.i(TAG, responseMessage);
						//networkRequestDidFinishWithResponse(methodId, jsonResponse);
					} catch (Exception e){
						e.printStackTrace();
						Error err = new Error(e);
						networkRequestDidFinishWithError(methodId, err);
					}
				}

				@Override
				public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable error) {
					Log.v(TAG, "get failed");
					Log.v("Status code: ", ""+statusCode);
					String responseMessage = "";
					if(responseBody != null){
						try {
							responseMessage = new String(responseBody, "UTF-8");
							Log.e("Response body: ", responseMessage.toString());
							Error err = new Error(responseMessage);
							networkRequestDidFinishWithError(methodId, err);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						Error err = new Error(error);
						networkRequestDidFinishWithError(methodId, err);
					}
				}
				//see http://loopj.com/android-async-http/doc/com/loopj/android/http/AsyncHttpResponseHandler.html for more callback methods
			});


		}
}

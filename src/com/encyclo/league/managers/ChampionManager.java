package com.encyclo.league.managers;

import org.json.JSONObject;

import com.loopj.android.http.RequestParams;

public class ChampionManager extends NetworkManager{

	private static ChampionManager mInstance = null;
	
	private ChampionManager(){
		super();
	}

	public static ChampionManager getInstance(){
		if(mInstance == null){
			mInstance = new ChampionManager();
		}
		return mInstance;
	}
	
	public void getChampions(){
		RequestParams params = null;
		String url = API_BASE_URL + DEFAULT_REGION + DEFAULT_VERSION + API_GET_CHAMPIONS + "?api_key="+API_KEY;
		makeGetServiceRequest(url, params, ChampionManagerGetChampions);
	}
	
	@Override
	public void networkRequestDidFinishWithResponse(int methodId,
			JSONObject responseJSON) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void networkRequestDidFinishWithError(int methodId, Error error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void networkRequestDidUpdateProgress(int methodId, int bytesWritten,
			int totalBytes) {
		// TODO Auto-generated method stub
		
	}

}

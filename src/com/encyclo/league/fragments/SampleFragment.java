package com.encyclo.league.fragments;

import org.json.JSONObject;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.encyclo.league.R;
import com.encyclo.league.managers.ChampionManager;
import com.encyclo.league.managers.NetworkManager.NetworkNotificationListener;

public class SampleFragment extends Fragment implements NetworkNotificationListener{
	private final static String TYPE_KEY="type";

	private static final String TAG = SampleFragment.class.getName();
	
	private String mType;
	
	private LinearLayout mLayout;
	private Button mButton;
	
	private ChampionManager mChampManager;
	
	public static final SampleFragment newInstance(String type)
	{
		SampleFragment f = new SampleFragment();
		Bundle bdl = new Bundle(1);
		bdl.putString(TYPE_KEY, type);
		f.setArguments(bdl);
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(getArguments()!=null && getArguments().containsKey(TYPE_KEY)){
			mType = getArguments().getString(TYPE_KEY);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mChampManager = ChampionManager.getInstance();
		
		mLayout = (LinearLayout) inflater.inflate(R.layout.fragment_sample, null, false);
		
		Log.v(TAG, "onCreateView");
		if(mType=="red") {
			mLayout.setBackgroundColor(Color.RED);
		} else if(mType=="blue") {
			mLayout.setBackgroundColor(Color.BLUE);
		} else if(mType=="green") {
			mLayout.setBackgroundColor(Color.GREEN);
		}
		
		mButton = (Button) mLayout.findViewById(R.id.button);
		mButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mChampManager.getChampions();
			}
		});
		return mLayout;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mChampManager.removeListener(this);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mChampManager.addListener(this);
	}

	@Override
	public void onSuccess(int methodId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSuccessWithResult(int methodId, JSONObject response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFailure(int methodId, Error error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccessDenied(int methodId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProgress(int methodId, int progress) {
		// TODO Auto-generated method stub
		
	}
}

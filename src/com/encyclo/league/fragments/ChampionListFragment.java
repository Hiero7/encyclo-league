package com.encyclo.league.fragments;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.encyclo.league.R;
import com.encyclo.league.managers.ChampionManager;
import com.encyclo.league.managers.NetworkManager.NetworkNotificationListener;

public class ChampionListFragment extends Fragment implements NetworkNotificationListener{

	private static final String TAG = ChampionListFragment.class.getName();
	
	private RelativeLayout mLayout = null;
	private GridView mChampionGrid = null;
	
	private ChampionManager mChampionManager = null;
	private ChampionListAdapter mGridAdapter = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		Log.v(TAG, "Creating Champion List view");
		
		mChampionManager = ChampionManager.getInstance();
		
		mChampionManager.addListener(this);
		mChampionManager.getChampions();
		
		mLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_champion_list, null, false);
		mChampionGrid = (GridView) mLayout.findViewById(R.id.champion_grid_view);
		mGridAdapter = new ChampionListAdapter(); 
		
		mChampionGrid.setAdapter(mGridAdapter);
		mGridAdapter.getView(0, null, null);
		
		mChampionGrid.setOnItemClickListener(new OnItemClickListener() {
			@Override
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
	            Toast.makeText(getActivity().getApplicationContext(), "" + position, Toast.LENGTH_SHORT).show();
	        }
	    });
	    
		return mLayout;
	}

	@Override
	public void onPause() {
		mChampionManager.removeListener(this);
		super.onPause();
	}

	@Override
	public void onResume() {
		mChampionManager.addListener(this);
		super.onResume();
	}
	
	
	//////////////////////////
	//Champion List Adapter
	//////////////////////////
	
	public class ChampionListAdapter extends BaseAdapter {
	    // references to our images
	    private Integer[] mThumbIds = {
	    		R.drawable.sample_2, R.drawable.sample_3,
	            R.drawable.sample_4, R.drawable.sample_5,
	            R.drawable.sample_6, R.drawable.sample_7,
	            R.drawable.sample_0, R.drawable.sample_1,
	            R.drawable.sample_2, R.drawable.sample_3,
	            R.drawable.sample_4, R.drawable.sample_5,
	            R.drawable.sample_6, R.drawable.sample_7,
	            R.drawable.sample_0, R.drawable.sample_1,
	            R.drawable.sample_2, R.drawable.sample_3,
	            R.drawable.sample_4, R.drawable.sample_5,
	            R.drawable.sample_6, R.drawable.sample_7
	    };
	    
	    public ChampionListAdapter() {
	    }

	    public int getCount() {
	    	Log.v(TAG, mThumbIds.length+"");
	        return mThumbIds.length;
	    }

	    public Object getItem(int position) {
	        return null;
	    }

	    public long getItemId(int position) {
	        return 0;
	    }

	    // create a new ImageView for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
	    	Log.v(TAG, "creating image view with " + mThumbIds[position]);
	        ImageView imageView;
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(getActivity().getApplicationContext());
	            imageView.setLayoutParams(new GridView.LayoutParams(160, 160));
	            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
	            imageView.setPadding(1, 1, 1, 1);
	        } else {
	            imageView = (ImageView) convertView;
	        }
	        imageView.setImageResource(mThumbIds[position]);
	        return imageView;
	    }
	}
	
	//////////////////////////
	//Network Response Methods
	//////////////////////////

	@Override
	public void onSuccess(int methodId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSuccessWithResult(int methodId, JSONObject response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFailure(int methodId, Error error) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccessDenied(int methodId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProgress(int methodId, int progress) {
		// TODO Auto-generated method stub
		
	}

}

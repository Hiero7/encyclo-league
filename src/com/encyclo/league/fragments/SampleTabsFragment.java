package com.encyclo.league.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.encyclo.league.R;
import com.viewpagerindicator.TabPageIndicator;
public class SampleTabsFragment extends Fragment {

private ArrayList<String> mTitles;

@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mTitles = new ArrayList<String>();

    mTitles.add(getString(R.string.title_1));
    mTitles.add(getString(R.string.title_2));
    mTitles.add(getString(R.string.title_3));
}

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_tabbed, null, false);

    FragmentPagerAdapter adapter = new SampleTabsAdapter(getChildFragmentManager());
    ViewPager pager = (ViewPager)view.findViewById(R.id.view_pager);
    if(pager!=null) {
        pager.setAdapter(adapter);
    }

    //note: the type of this indicator is dependent on the style of paging you want.  
    //the library I mentioned has multiple options in this regard
    TabPageIndicator indicator = (TabPageIndicator)view.findViewById(R.id.indicator);
    indicator.setViewPager(pager);
    indicator.setOnPageChangeListener(new OnPageChangeListener() {

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // TODO Auto-generated method stub

        }

		@Override
		public void onPageSelected(int arg0) {
			// TODO Auto-generated method stub
			
		}
    });
    return view;
}

class SampleTabsAdapter extends FragmentPagerAdapter {
    public SampleTabsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch(position){

        case 0:
            fragment = SampleFragment.newInstance("red");
            break;
        case 1:
        	fragment = SampleFragment.newInstance("blue");
            break;
        case 2:
        	fragment = SampleFragment.newInstance("green");
        default:
            break;
        }

        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position % mTitles.size()).toUpperCase();
    }

    @Override
    public int getCount() {
        return mTitles.size();
    }
}

}
